package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"
)

type Options struct {
	port int
	path string
}

func main() {
	options := Options{
		port: 8080,
		path: ".",
	}

	flag.IntVar(&options.port, "port", options.port, "webserver port")
	flag.StringVar(&options.path, "path", options.path, "path to serve files")
	flag.Parse()

	if _, err := os.Stat(options.path); os.IsNotExist(err) {
		fmt.Printf("Error! path not found: %s", options.path)
		return
	}

	handler := http.FileServer(http.Dir(options.path))
	handler = logMiddleware(handler)

	fmt.Printf("http://localhost:%d\n", options.port)
	fmt.Println("Serving files from:", options.path)
	panic(http.ListenAndServe(fmt.Sprintf(":%d", options.port), handler))
}

func logMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf(
			"[%s] %s %s\n",
			time.Now().Format("2006/01/02 15:04:05"),
			r.Method,
			r.URL.String(),
		)

		h.ServeHTTP(w, r)
	})
}
